import os

import numpy as np
from flask import Flask, jsonify, request

from index import Index

INDEX_PATH = os.getenv('INDEX_PATH')
EMBEDDING_PATH = os.getenv('EMBEDDING_PATH')

index = Index(INDEX_PATH, EMBEDDING_PATH)

app = Flask(__name__)


@app.route('/health', methods=['GET'])
def health():
    return jsonify({'status': 'ok'})


@app.route('/get_k_nearest', methods=['POST'])
def get_k_nearest():
    data = request.json
    k = data['k']
    cluster = data['cluster']
    embedding = np.array(data['embedding'])

    try:
        k_nearest = index.get_k_nearest(k, embedding, cluster)
        return jsonify({'k_nearest': k_nearest})

    except Exception as e:
        return jsonify({'error': str(e)}), 500


if __name__ == '__main__':
    app.run('0.0.0.0', 6000)
