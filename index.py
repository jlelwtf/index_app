import json
import pickle

import faiss
import numpy as np


class Index:
    def __init__(
            self,
            index_path: str,
            cluster_embeddings_path: str
    ):
        self.cluster_embeddings_path = cluster_embeddings_path
        self.index_path = index_path
        self.index = None
        self.embeddings = None
        self.faiss_index = None
        self._load_index()
        self._create_faiss_index()

    def _load_index(self):
        with open(self.index_path, 'r') as f:
            self.index = json.load(f)
        with open(self.cluster_embeddings_path, 'rb') as f:
            self.embeddings = pickle.load(f)

    def _create_faiss_index(self):
        self.faiss_index = {}
        for cluster in self.embeddings:
            embedding = self.embeddings[cluster].astype(np.float32)
            faiss.normalize_L2(embedding)
            index = faiss.IndexFlatIP(embedding.shape[1])
            index.add(embedding)
            self.faiss_index[cluster] = index

    def get_k_nearest(self, k, embedding, cluster):
        if cluster not in self.faiss_index:
            raise Exception('cluster not found')
        _, indices = self.faiss_index[cluster].search(
            np.expand_dims(embedding, 0).astype(np.float32), k)
        k_nearest = []
        for idx in indices[0]:
            k_nearest.append(self.index[cluster][idx])
        return k_nearest






