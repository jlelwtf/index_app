import argparse
import json
import pickle

from typing import List
import tensorflow as tf
import grpc
import numpy as np
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2_grpc


MAX_MESSAGE_LENGTH = 100000000
REQUEST_TIMEOUT = 30
BATCH_SIZE = 10000


class EmbeddingLoader:
    def __init__(
            self,
            embedder_address: str,
            batch_size=BATCH_SIZE,
            max_message_length=MAX_MESSAGE_LENGTH,
            request_timeout=REQUEST_TIMEOUT,
    ):
        self.request_timeout = request_timeout
        self.batch_size = batch_size
        self.channel = grpc.insecure_channel(
            embedder_address,
            options=[
                ("grpc.max_send_message_length", max_message_length),
                ("grpc.max_receive_message_length", max_message_length),
            ],
        )
        self.stub = prediction_service_pb2_grpc.PredictionServiceStub(self.channel)

    def load_embeddings(self, sentences: List[str]) -> np.ndarray:
        res = []
        req = predict_pb2.PredictRequest()
        req.model_spec.name = 'use'
        req.model_spec.signature_name = ''

        for idx in range(0, len(sentences), self.batch_size):
            batch = sentences[idx: idx + self.batch_size]
            tensor = tf.make_tensor_proto(batch)
            req.inputs["text"].CopyFrom(tensor)
            resp = self.stub.Predict(req, self.request_timeout)
            res.extend(list(resp.outputs["embedding"].float_val))

        return np.array(res).reshape((len(sentences), 512))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="embedding loader")

    parser.add_argument(
        '--embedder_address',
        type=str,
        required=True,
    )

    parser.add_argument(
        '--embeddings_path',
        type=str,
        required=True
    )

    parser.add_argument(
        '--index_path',
        type=str,
        required=True
    )

    args = parser.parse_args()
    embedder_address = args.embedder_address
    embeddings_path = args.embeddings_path
    index_path = args.index_path

    with open(index_path, 'r') as f:
        index = json.load(f)

    embedding_loader = EmbeddingLoader(embedder_address)
    embeddings = {}
    for cluster in index:
        embeddings[cluster] = embedding_loader.load_embeddings(index[cluster])

    with open(embeddings_path, 'wb') as f:
        pickle.dump(embeddings, f)
