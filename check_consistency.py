import argparse
import json
import pickle

parser = argparse.ArgumentParser(description="embedding loader")

parser.add_argument(
    '--clusters_centers_path',
    type=str,
    required=True
)

parser.add_argument(
    '--index_path',
    type=str,
    required=True
)

if __name__ == '__main__':
    args = parser.parse_args()
    index_path = args.index_path
    clusters_centers_path = args.clusters_centers_path

    with open(index_path, 'r') as f:
        index = json.load(f)

    with open(clusters_centers_path, 'rb') as f:
        clusters_centers = pickle.load(f)

    clusters_1 = set(index.keys())
    clusters_2 = set(clusters_centers)
    if clusters_1 != clusters_2:
        raise Exception(f'Разный набор кластеров.\n{clusters_1} в {index_path}\n {clusters_2} в {clusters_centers_path}')